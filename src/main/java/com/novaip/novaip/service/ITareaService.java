package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.TareaDto;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface ITareaService {

    TareaDto create(TareaDto tareaDto);
    TareaDto update(TareaDto tareaDto);
    Optional<TareaDto> findById(Integer id);
    Page<TareaDto> findAll(Integer pageSize, Integer pageNumber);
    void delete(Integer id);
}
