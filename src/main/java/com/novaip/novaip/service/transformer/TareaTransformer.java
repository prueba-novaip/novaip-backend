package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Tarea;
import com.novaip.novaip.service.dto.TareaDto;

public class TareaTransformer {
    public static TareaDto getTareaDtoFromTarea(Tarea tarea){

        if(tarea == null){
            return null;
        }
        TareaDto dto = new TareaDto();

        dto.setId(tarea.getId());
        dto.setDescripcion(tarea.getDescripcion());
        dto.setAlias(tarea.getAlias());
        dto.setEliminado(tarea.getEliminado());
        dto.setEstado(tarea.getEstado());
        dto.setFechaActualizacion(tarea.getFechaActualizacion());
        dto.setFechaCreacion(tarea.getFechaCreacion());
        dto.setNombre(tarea.getNombre());
        dto.setFechaInicio(tarea.getFechaInicio());
        dto.setFechaFin(tarea.getFechaFin());

        return dto;
    }

    public static Tarea getTareaFromTareaDto(TareaDto dto){

        if (dto == null) {
            return null;
        }

        Tarea tarea = new Tarea();

        tarea.setId(dto.getId());
        tarea.setDescripcion(dto.getDescripcion());
        tarea.setAlias(dto.getAlias());
        tarea.setEliminado(dto.getEliminado());
        tarea.setEstado(dto.getEstado());
        tarea.setFechaActualizacion(dto.getFechaActualizacion());
        tarea.setFechaCreacion(dto.getFechaCreacion());
        tarea.setNombre(dto.getNombre());
        tarea.setFechaInicio(dto.getFechaInicio());
        tarea.setFechaFin(dto.getFechaFin());

        return tarea;
    }
}
