package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Usuario;
import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsuariosDto;

public class UsuarioTransformer {
    public static UsuariosDto getUsuarioDtoFromUsuario(Usuario usuario){

        if(usuario == null){
            return  null;
        }
        UsuariosDto dto = new UsuariosDto();

        dto.setId(usuario.getId());
        dto.setCedula(usuario.getCedula());
        dto.setEliminado(usuario.getEliminado());
        dto.setContraseña(usuario.getContraseña());
        dto.setEmail(usuario.getEmail());
        dto.setEstado(usuario.getEstado());
        dto.setFechaCreacion(usuario.getFechaCreacion());
        dto.setNombre(usuario.getNombre());
        dto.setFechaActualizacion(usuario.getFechaActualizacion());
        dto.setProyecto(usuario.getProyecto());
        dto.setRols(usuario.getRols());
        dto.setTarea(usuario.getTarea());

        return dto;
    }

    public static Usuario getUsuarioFromUsuarioDto(UsuariosDto dto){
        if (dto == null) {
            return null;
        }

        Usuario usuario = new Usuario();

        usuario.setId(dto.getId());
        usuario.setCedula(dto.getCedula());
        usuario.setEliminado(dto.getEliminado());
        usuario.setContraseña(dto.getContraseña());
        usuario.setEmail(dto.getEmail());
        usuario.setEstado(dto.getEstado());
        usuario.setFechaCreacion(dto.getFechaCreacion());
        usuario.setNombre(dto.getNombre());
        usuario.setFechaActualizacion(dto.getFechaActualizacion());
        usuario.setProyecto(dto.getProyecto());
        usuario.setRols(dto.getRols());
        usuario.setTarea(dto.getTarea());

        return usuario;
    }

    public static LoginInformation getLoginInformationFromUsers(Usuario usuario){
        if (usuario == null){
            return null;
        }

        LoginInformation dto = new LoginInformation();

        dto.setIdUser(usuario.getId());
        dto.setCedula(usuario.getCedula());
        dto.setNombre(usuario.getNombre());
        dto.setEstado(usuario.getEstado());
        dto.setEmail(usuario.getEmail());
        dto.setRols(usuario.getRols());

        return dto;
    }
}
