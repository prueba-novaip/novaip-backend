package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Proyecto;
import com.novaip.novaip.service.dto.ProyectoDto;

public class ProyectoTransformer {
    public static ProyectoDto getProyectoDtoFromProyecto(Proyecto proyecto){

        if(proyecto == null){
            return null;
        }
        ProyectoDto dto = new ProyectoDto();

        dto.setId(proyecto.getId());
        dto.setDescripcion(proyecto.getDescripcion());
        dto.setAlias(proyecto.getAlias());
        dto.setEliminado(proyecto.getEliminado());
        dto.setEstado(proyecto.getEstado());
        dto.setFechaActualizacion(proyecto.getFechaActualizacion());
        dto.setFechaCreacion(proyecto.getFechaCreacion());
        dto.setNombre(proyecto.getNombre());
        dto.setFechaInicio(proyecto.getFechaInicio());
        dto.setFechaFin(proyecto.getFechaFin());
        dto.setIdTarea(proyecto.getTarea().getId());

        return dto;
    }

    public static Proyecto getProyactoFromProyectoDto(ProyectoDto dto){

        if (dto == null) {
            return null;
        }

        Proyecto proyecto = new Proyecto();

        proyecto.setId(dto.getId());
        proyecto.setDescripcion(dto.getDescripcion());
        proyecto.setAlias(dto.getAlias());
        proyecto.setEliminado(dto.getEliminado());
        proyecto.setEstado(dto.getEstado());
        proyecto.setFechaActualizacion(dto.getFechaActualizacion());
        proyecto.setFechaCreacion(dto.getFechaCreacion());
        proyecto.setNombre(dto.getNombre());
        proyecto.setFechaInicio(dto.getFechaInicio());
        proyecto.setFechaFin(dto.getFechaFin());

        return proyecto;
    }
}
