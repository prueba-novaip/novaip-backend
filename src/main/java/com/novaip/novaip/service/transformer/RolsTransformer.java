package com.novaip.novaip.service.transformer;

import com.novaip.novaip.domain.Rols;
import com.novaip.novaip.service.dto.RolsDto;

public class RolsTransformer {
    public static RolsDto getRolsDTOFromRols(Rols rols) {

        if (rols == null) {
            return null;
        }
        RolsDto dto = new RolsDto();

        //set variables
        dto.setIdRols(rols.getIdRols());
        dto.setDescription(rols.getDescription());



        return dto;
    }
    public static Rols getRolsFromRolsDTO(RolsDto dto){
        if (dto == null) {
            return null;
        }

        Rols rols = new Rols();

        rols.setIdRols(dto.getIdRols());
        rols.setDescription(dto.getDescription());

        return rols;
    }
}
