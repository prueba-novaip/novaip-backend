package com.novaip.novaip.service.dto;

import com.novaip.novaip.domain.Proyecto;
import com.novaip.novaip.domain.Rols;
import com.novaip.novaip.domain.Tarea;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class UsuariosDto {

    @Id
    private int id;

    private String cedula;
    private String nombre;
    private String email;
    private String contraseña;
    private Boolean estado;
    private Boolean eliminado;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;

    private Set<Proyecto> proyecto = new HashSet<>();

    private Set<Tarea> tarea = new HashSet<>();

    private Set<Rols> rols = new HashSet<>();
}
