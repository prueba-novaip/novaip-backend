package com.novaip.novaip.service.dto;

import com.novaip.novaip.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class LoginInformation implements Serializable {
    @Id
    private Integer idUser;

    private String cedula;

    private Boolean estado;

    private String nombre;

    private String email;

    private Set<Rols> rols;

    private List<String> authorities;
}
