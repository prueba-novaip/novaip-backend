package com.novaip.novaip.service.dto;

import com.novaip.novaip.domain.Proyecto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
public class TareaDto {

    @Id
    private int id;

    private String nombre;
    private String descripcion;

    @Column(unique = true)
    private String alias;

    private Boolean estado;
    private Boolean eliminado;

    private LocalDate fechaInicio;

    private LocalDate fechaFin;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;

}
