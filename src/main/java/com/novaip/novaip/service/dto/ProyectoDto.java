package com.novaip.novaip.service.dto;

import com.novaip.novaip.domain.Tarea;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
public class ProyectoDto {

    @Id
    private int id;

    private String nombre;
    private String descripcion;
    private String alias;
    private Boolean estado;
    private Boolean eliminado;

    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private LocalDate fechaCreacion;
    private LocalDate fechaActualizacion;

    private int IdTarea;
}
