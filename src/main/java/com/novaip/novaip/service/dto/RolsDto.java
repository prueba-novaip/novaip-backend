package com.novaip.novaip.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

@Getter
@Setter
public class RolsDto {

    @Id
    private Integer idRols;
    private String description;
}
