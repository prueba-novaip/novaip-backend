package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.ProyectoDto;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface IProyectoService {

    ProyectoDto create(ProyectoDto proyectoDto);
    ProyectoDto update(ProyectoDto proyectoDto);
    Optional<ProyectoDto> findById(Integer id);
    Page<ProyectoDto> findAll(Integer pageSize, Integer pageNumber);
    void delete(Integer id);
}
