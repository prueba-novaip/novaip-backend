package com.novaip.novaip.service.impl;

import com.novaip.novaip.domain.Tarea;
import com.novaip.novaip.repository.TareaRepository;
import com.novaip.novaip.service.ITareaService;
import com.novaip.novaip.service.dto.TareaDto;
import com.novaip.novaip.service.transformer.TareaTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ITareaServiceImpl implements ITareaService {

    @Autowired
    TareaRepository repository;

    @Override
    public TareaDto create(TareaDto tareaDto) {
        Tarea tarea = TareaTransformer.getTareaFromTareaDto(tareaDto);
        return TareaTransformer.getTareaDtoFromTarea(repository.save(tarea));
    }

    @Override
    public TareaDto update(TareaDto tareaDto) {
        Tarea tarea = TareaTransformer.getTareaFromTareaDto(tareaDto);
        return TareaTransformer.getTareaDtoFromTarea(repository.save(tarea));
    }

    @Override
    public Optional<TareaDto> findById(Integer id) {
        return repository.findById(id)
                .map(TareaTransformer::getTareaDtoFromTarea);
    }

    @Override
    public Page<TareaDto> findAll(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(TareaTransformer::getTareaDtoFromTarea);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
