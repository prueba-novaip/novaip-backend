package com.novaip.novaip.service.impl;

import com.novaip.novaip.domain.Usuario;
import com.novaip.novaip.repository.UsuarioRepository;
import com.novaip.novaip.service.IUsuarioService;
import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsuariosDto;
import com.novaip.novaip.service.transformer.UsuarioTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class IUsuariosServiceImpl implements IUsuarioService {

    private Logger logger = LoggerFactory.getLogger(IUsuariosServiceImpl.class);

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    UsuarioRepository repository;

    @Override
    public UsuariosDto create(UsuariosDto usuariosDto) {
        Usuario usuario = UsuarioTransformer.getUsuarioFromUsuarioDto(usuariosDto);
        usuario.setContraseña(passwordEncoder.encode(usuario.getContraseña()));
        return UsuarioTransformer.getUsuarioDtoFromUsuario(repository.save(usuario));
    }

    @Override
    public UsuariosDto update(UsuariosDto usuariosDto) {
        Usuario usuario = UsuarioTransformer.getUsuarioFromUsuarioDto(usuariosDto);
        return UsuarioTransformer.getUsuarioDtoFromUsuario(repository.save(usuario));
    }

    @Override
    public Optional<UsuariosDto> findById(Integer id) {
        return repository.findById(id)
                .map(UsuarioTransformer::getUsuarioDtoFromUsuario);
    }

    @Override
    public Page<UsuariosDto> findAll(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(UsuarioTransformer::getUsuarioDtoFromUsuario);
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public LoginInformation findByEmail(String username) throws UsernameNotFoundException {
        Usuario usuario = repository.findByEmail(username);

        if(usuario == null){
            logger.error("Error: Email "+ username + " no existe");
            throw new UsernameNotFoundException("Error: Email "+ username + " no existe");
        }
        LoginInformation loginInformation = UsuarioTransformer.getLoginInformationFromUsers(usuario);

        ArrayList<String> authorities = new ArrayList<String>();
        loginInformation.getRols().forEach(item ->{
            authorities.add(item.getDescription());
        });
        loginInformation.setAuthorities(authorities);
        return loginInformation;
    }
}
