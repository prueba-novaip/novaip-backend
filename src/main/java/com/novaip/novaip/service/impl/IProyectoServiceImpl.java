package com.novaip.novaip.service.impl;

import com.novaip.novaip.domain.Proyecto;
import com.novaip.novaip.domain.Tarea;
import com.novaip.novaip.repository.ProyectoRepository;
import com.novaip.novaip.repository.TareaRepository;
import com.novaip.novaip.service.IProyectoService;
import com.novaip.novaip.service.dto.ProyectoDto;
import com.novaip.novaip.service.transformer.ProyectoTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IProyectoServiceImpl implements IProyectoService {

    @Autowired
    ProyectoRepository repository;

    @Autowired
    TareaRepository tareaRepository;

    @Override
    public ProyectoDto create(ProyectoDto proyectoDto) {
        Proyecto proyecto = ProyectoTransformer.getProyactoFromProyectoDto(proyectoDto);
        Tarea tarea = tareaRepository.findById(proyectoDto.getIdTarea()).get();
        proyecto.setTarea(tarea);
        return ProyectoTransformer.getProyectoDtoFromProyecto(repository.save(proyecto));
    }

    @Override
    public ProyectoDto update(ProyectoDto proyectoDto) {
        Proyecto proyecto = ProyectoTransformer.getProyactoFromProyectoDto(proyectoDto);
        Tarea tarea = tareaRepository.findById(proyectoDto.getIdTarea()).get();
        proyecto.setTarea(tarea);
        return ProyectoTransformer.getProyectoDtoFromProyecto(repository.save(proyecto));
    }

    @Override
    public Optional<ProyectoDto> findById(Integer id) {
        return repository.findById(id)
                .map(ProyectoTransformer::getProyectoDtoFromProyecto);
    }

    @Override
    public Page<ProyectoDto> findAll(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return repository.findAll(pageable)
                .map(ProyectoTransformer::getProyectoDtoFromProyecto);

    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
