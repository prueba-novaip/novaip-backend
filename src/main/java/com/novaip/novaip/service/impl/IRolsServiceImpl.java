package com.novaip.novaip.service.impl;

import com.novaip.novaip.domain.Rols;
import com.novaip.novaip.repository.RolsRepository;
import com.novaip.novaip.service.IRolsService;
import com.novaip.novaip.service.dto.RolsDto;
import com.novaip.novaip.service.transformer.RolsTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IRolsServiceImpl implements IRolsService {

    @Autowired
    RolsRepository repository;

    @Override
    public RolsDto create(RolsDto rolsDto) {
        Rols rols = RolsTransformer.getRolsFromRolsDTO(rolsDto);
        return RolsTransformer.getRolsDTOFromRols(repository.save(rols));
    }

    @Override
    public RolsDto update(RolsDto rolsDto) {
        Rols rols = RolsTransformer.getRolsFromRolsDTO(rolsDto);
        return RolsTransformer.getRolsDTOFromRols(repository.save(rols));
    }

    @Override
    public Optional<RolsDto> findById(Integer id) {
        return repository.findById(id)
                .map(RolsTransformer::getRolsDTOFromRols);
    }

    @Override
    public List<RolsDto> findAll() {
         return repository.findAll().stream().map(RolsTransformer::getRolsDTOFromRols).collect(Collectors.toList());
    }


    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
