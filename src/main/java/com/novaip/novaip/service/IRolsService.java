package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.RolsDto;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface IRolsService {

    RolsDto create(RolsDto rolsDto);
    RolsDto update(RolsDto rolsDto);
    Optional<RolsDto> findById(Integer id);
    List<RolsDto> findAll();
    void delete(Integer id);
}
