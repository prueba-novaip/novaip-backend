package com.novaip.novaip.service;

import com.novaip.novaip.domain.Usuario;
import com.novaip.novaip.repository.UsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private UsuarioRepository usuarioRepository;

    //method that returns user authorizations
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
        Usuario usuario = usuarioRepository.findByEmail(email);

        if (usuario == null){
            logger.error("Error en el login: no existe el usuario '" + email + "'");
            throw new UsernameNotFoundException("Error en el login: no existe el usuario'" + email + "'");
        }
        List<GrantedAuthority> authorities = usuario.getRols()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getDescription()))
                .peek(authority -> logger.info("Role: " + authority.getAuthority()))
                .collect(Collectors.toList());
        return new User(usuario.getEmail(), usuario.getContraseña(), usuario.getEstado(), true, true, true, authorities);

    }

}
