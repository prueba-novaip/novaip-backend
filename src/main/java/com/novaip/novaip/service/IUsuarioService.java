package com.novaip.novaip.service;

import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsuariosDto;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public interface IUsuarioService {

    UsuariosDto create(UsuariosDto usuariosDto);
    UsuariosDto update(UsuariosDto usuariosDto);
    Optional<UsuariosDto> findById(Integer id);
    Page<UsuariosDto> findAll(Integer pageSize, Integer pageNumber);
    void delete(Integer id);
    LoginInformation findByEmail (String username) throws UsernameNotFoundException;
}
