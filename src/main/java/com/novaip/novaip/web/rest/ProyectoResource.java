package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.IProyectoService;
import com.novaip.novaip.service.dto.ProyectoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/proyecto")
public class ProyectoResource {

    @Autowired
    IProyectoService service;

    @PostMapping("")
    public ProyectoDto create(@RequestBody ProyectoDto proyectoDto) {
        return service.create(proyectoDto);
    }

    @PutMapping("")
    public ProyectoDto update(@RequestBody ProyectoDto proyectoDto) {
        return service.update(proyectoDto);
    }

    @GetMapping("")
    public Page<ProyectoDto> findAll(@RequestParam(value = "pageSize")Integer pageSize,
                                     @RequestParam(value = "pageNumber") Integer pageNumber) {
        return service.findAll(pageSize, pageNumber);
    }

    @GetMapping("/{id}")
    public Optional<ProyectoDto> findById(@PathVariable Integer id) {
        return service.findById(id);
    }

    @DeleteMapping("delete/{id}")
    public void delete(Integer id) {
        service.delete(id);
    }
}
