package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.IRolsService;
import com.novaip.novaip.service.dto.RolsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "localhost:4200")
@RequestMapping("/api/rols")
public class RolsResource {

    @Autowired
    IRolsService service;

    @PostMapping("")
    public RolsDto create(@RequestBody RolsDto rolsDto) {
        return service.create(rolsDto);
    }

    @PutMapping("")
    public RolsDto update(@RequestBody RolsDto rolsDto) {
        return service.update(rolsDto);
    }

    @GetMapping("")
    public List<RolsDto> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Optional<RolsDto> findById(@PathVariable Integer id) {
        return service.findById(id);
    }
    @DeleteMapping("/{id}")
    public void delete(Integer id) {
        service.delete(id);
    }
}
