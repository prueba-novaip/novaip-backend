package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.IUsuarioService;
import com.novaip.novaip.service.dto.LoginInformation;
import com.novaip.novaip.service.dto.UsuariosDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioResource {

    @Autowired
    IUsuarioService service;

    @PostMapping("")
    public UsuariosDto create(@RequestBody UsuariosDto usuariosDto) {
        return service.create(usuariosDto);
    }

    @PutMapping("")
    public UsuariosDto update(@RequestBody UsuariosDto usuariosDto) {
        return service.update(usuariosDto);
    }

    @GetMapping("")
    public Page<UsuariosDto> findAll(@RequestParam(value = "pageSize")Integer pageSize,
                                  @RequestParam(value = "pageNumber") Integer pageNumber) {
        return service.findAll(pageSize, pageNumber);
    }

    @GetMapping("/{id}")
    public Optional<UsuariosDto> findById(@PathVariable Integer id) {
        return service.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        service.delete(id);
    }

    @GetMapping("/account")
    public ResponseEntity<LoginInformation> getAccount(@RequestParam(value = "username") String username){
        return new ResponseEntity<>(service.findByEmail(username), HttpStatus.OK);
    }
}
