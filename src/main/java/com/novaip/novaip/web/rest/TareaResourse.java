package com.novaip.novaip.web.rest;

import com.novaip.novaip.service.ITareaService;
import com.novaip.novaip.service.dto.TareaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/tarea")
public class TareaResourse {

    @Autowired
    ITareaService service;

    @PostMapping("")
    public TareaDto create(@RequestBody TareaDto tareaDto) {
        return service.create(tareaDto);
    }

    @PutMapping("")
    public TareaDto update(@RequestBody TareaDto tareaDto) {
        return service.update(tareaDto);
    }

    @GetMapping("")
    public Page<TareaDto> findAll(@RequestParam(value = "pageSize")Integer pageSize,
                                 @RequestParam(value = "pageNumber") Integer pageNumber) {
        return service.findAll(pageSize, pageNumber);
    }

    @GetMapping("/{id}")
    public Optional<TareaDto> findById(@PathVariable Integer id) {
        return service.findById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(Integer id) {
        service.delete(id);
    }
}
