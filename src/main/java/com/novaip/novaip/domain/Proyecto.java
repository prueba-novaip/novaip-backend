package com.novaip.novaip.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Getter
@Setter
public class Proyecto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String nombre;
    private String descripcion;
    private String alias;
    private Boolean estado;
    private Boolean eliminado;

    private LocalDate fechaInicio;

    private LocalDate fechaFin;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;

    @ManyToOne
    private Tarea tarea;
}
