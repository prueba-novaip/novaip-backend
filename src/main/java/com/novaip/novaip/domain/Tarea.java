package com.novaip.novaip.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
public class Tarea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String nombre;
    private String descripcion;

    @Column(unique = true)
    private String alias;

    private Boolean estado;
    private Boolean eliminado;

    private LocalDate fechaInicio;

    private LocalDate fechaFin;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;


}
