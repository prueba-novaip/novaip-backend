package com.novaip.novaip.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String cedula;
    private String nombre;
    private String email;
    private String contraseña;
    private Boolean estado;
    private Boolean eliminado;

    private LocalDate fechaCreacion;

    private LocalDate fechaActualizacion;

    @ManyToMany
    @JoinTable(name = "usuario_has_proyecto",
            joinColumns = @JoinColumn(name = "id_usuario"),
            inverseJoinColumns = @JoinColumn(name = "id_proyecto"))
    private Set<Proyecto> proyecto = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "usuario_has_tarea",
            joinColumns = @JoinColumn(name = "id_usuario"),
            inverseJoinColumns = @JoinColumn(name = "id_tarea"))
    private Set<Tarea> tarea = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "user_has_rol",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private Set<Rols> rols = new HashSet<>();
}
