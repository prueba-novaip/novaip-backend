package com.novaip.novaip.repository;

import com.novaip.novaip.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    Usuario findByEmail (String email);
}
