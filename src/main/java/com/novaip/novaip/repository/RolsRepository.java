package com.novaip.novaip.repository;

import com.novaip.novaip.domain.Rols;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolsRepository extends JpaRepository<Rols, Integer> {
}
