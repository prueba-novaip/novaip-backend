package com.novaip.novaip.auth;

public class JwtConfig {
    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEogIBAAKCAQEArPmNIcAOj2rJnKq36Swv8g3U946wP1peLMIjAUjTN6bvnjlS\n" +
            "ijqodaM9NayWiHr+B8cm7tg6rSkfPmseIsBcFgzPtM3XeLEbp3j9QbLxgRm++zvx\n" +
            "75ZUWjrlVPejENk/CflWV9Bw5VCYB405fSVLXtcaUP6QudXcStZ61rf/KhsQ5TpP\n" +
            "yMEMcQ3z14q5A0qpdGNbZHFFzhfPAQgHfVZTTkTfucxR3nC1u0qzhk66b4Je3XWA\n" +
            "iMJvHZflURR7d3Pak0PxzoOmaRctEI4jz8M7u7WGUtpMWlKq/ftSOkrzuk98VAYq\n" +
            "q3uxx1u058sEGL8BBIPr+tJHvPWOsYA57Yts4QIDAQABAoIBAASnNe+5ferHwaKw\n" +
            "SQBBywxk6Ny7/uvFX5Euq8hmECCIIhwMiLblOxDnEkd2XBpRzLGi3t4//NwZphiE\n" +
            "wyvGJvQE5jWD8A70HL1VmOKCkNKbQbL6etiqH5zJxs+RVdanMXXwOi1kkxToJR7W\n" +
            "4rwGG8/yU2foYTZOlX2B5Y4PJxMX2fpdrXq8Wr3ft92wVrwdvfTZExnMndbid4mY\n" +
            "mTPqV8z77JH3LxBRcEtK4DFJXtrzYCY03yaVgMnDCNmluCBnjhT+8HHvORUdN0D0\n" +
            "lZ9/V3ZjSSxRWN/bGht1TZyK43eUDniTVHhKyedZ+ktl9b3zScAFY/it3tBYWkEr\n" +
            "cOVz7o0CgYEA11RGZ8myrRyQaxynB1KFAKuKWmGHwLRffZnjesyFWxOSIB5w6RR9\n" +
            "LTDfBVHTUSitnlrpY0giqQZ81sL+xH4X3TDl6LpTA3r30//DaCvD3ij1+YSk1LEn\n" +
            "EhDjPRcp8MJpSOw6MrF3b0Hv6IbU70CkjnbU0ntNwWhCo6DpNNpjVXcCgYEAzaVU\n" +
            "7U8W3snlZRhffoL2cDaeo18DtawLUKrvSaz2LxHvXVFWUqT+6nMEg1hpYozuusMp\n" +
            "UlUA3P6XaRCwP3NhJYbPqyX0FSi+JqdlJjXvizUgUlg5poiAjICQmCLpQC2MvwC6\n" +
            "iYdpJnnSXa0ocqkyczwOpZzfWk1E6M+U1QbWxmcCgYAFtC3JQgaBn2gT7kZeY0ky\n" +
            "o1ie3EaETELbSOBFuHUv3GzafPPwuKI4ODswIOLAG5u9vhrAjoCciZd0+OVWvyBS\n" +
            "s6DjcUwRo4g9DWFXuREPKXaajXDwUWD/kYLzc1GhFwVC8T75aMoJIG4Ccs/hJo01\n" +
            "WOB6UtnUbbaWLwT+aWqIMQKBgDR5crFXVBunYwyn6n4qxt2dYl+HuDecXgrIg2TG\n" +
            "aM9cATtLrP5Lsex7SvSFE+cc1TH86Mdkf2Lk6BfcNU0LkQd1XfprJdxcWCEe59WX\n" +
            "P7fC56t7bcKL5Fl12O+pQOGqfdajRfYgAnnZ7g8PR1nyyLfU5FvoKqotO1840N6G\n" +
            "7zkTAoGABUdgJiy39ivJc2HYXXDXavtDXh1hUXVpCPb8eazcei6rnENZy9A+feVz\n" +
            "WDFSa/s9Ry4oDjjZVOJeSrrPf/38Gu6hIMdu9FcYwMh0+dHovSuA+VQ7xHute1ej\n" +
            "Xs1PSCXCn8p+HS3+7P1fPd8Ty2+AZls5R5+q/MYbDgYY+zq3DOU=\n" +
            "-----END RSA PRIVATE KEY-----";
    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArPmNIcAOj2rJnKq36Swv\n" +
            "8g3U946wP1peLMIjAUjTN6bvnjlSijqodaM9NayWiHr+B8cm7tg6rSkfPmseIsBc\n" +
            "FgzPtM3XeLEbp3j9QbLxgRm++zvx75ZUWjrlVPejENk/CflWV9Bw5VCYB405fSVL\n" +
            "XtcaUP6QudXcStZ61rf/KhsQ5TpPyMEMcQ3z14q5A0qpdGNbZHFFzhfPAQgHfVZT\n" +
            "TkTfucxR3nC1u0qzhk66b4Je3XWAiMJvHZflURR7d3Pak0PxzoOmaRctEI4jz8M7\n" +
            "u7WGUtpMWlKq/ftSOkrzuk98VAYqq3uxx1u058sEGL8BBIPr+tJHvPWOsYA57Yts\n" +
            "4QIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
